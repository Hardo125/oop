import java.text.DecimalFormat;

/**
 * Created by Villem on 01/05/2017.
 */
public class PoissonJaotus implements Jaotused {
    private int lambda;
    private int täpne;
    private DecimalFormat df = new DecimalFormat("#.###");

    public PoissonJaotus(int lambda) {
        this.lambda = lambda;
    }

    public PoissonJaotus(int lambda, int täpne) {
        this.lambda = lambda;
        this.täpne = täpne;
    }

    @Override
    public double Keskväärtus() {
        return lambda;
    }

    @Override
    public double Dispersioon() {
        return lambda;
    }

    @Override
    public double Standardhälve() {
        return Double.parseDouble(df.format(Math.sqrt(this.Dispersioon())));
    }

    @Override
    public double Täpne() {
        double vastus = Math.pow(lambda, täpne) / faktoriaal(täpne) * Math.exp(-lambda);
        return Double.parseDouble(df.format(vastus));
    }

    private long faktoriaal(int i){
        long kokku = 1;
        for(int k = i; k > 0; k--) {
            kokku = kokku*k;
        }
        return kokku;
    }
}
