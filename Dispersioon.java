import java.util.ArrayList;
import java.util.List;

/**
 * Created by Villem Lassmann on 10/03/2017.
 */
public class Dispersioon implements Arvutused{
    private String nimi = "dispersioon";
    private Keskväärtus kv = new Keskväärtus();

    public String getNimi() {
        return nimi;
    }

    public double arvutus(List<Double> suurused, List<Double> tõenäosused){
        double tulemus;
        ArrayList<Double> ruudustus = new ArrayList<>();
        for (double väärtus: suurused){
            ruudustus.add(Math.pow(väärtus, 2.0));
        }
        tulemus = Math.round((kv.arvutus(ruudustus, tõenäosused) - Math.pow(kv.arvutus(suurused, tõenäosused), 2.0))*1000);
        return tulemus/1000;
    }

}
