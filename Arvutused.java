import java.util.List;

/**
 * Created by Villem Lassmann on 09/03/2017.
 */
public interface Arvutused {
    String getNimi();
    double arvutus(List<Double> suurused, List<Double> tõenäosused);

}
