import java.text.DecimalFormat;

/**
 * Created by Villem on 01/05/2017.
 */
public class GeomeetrilineJaotus implements Jaotused{
    private double tõenäosus;
    private int täpne;
    private DecimalFormat df = new DecimalFormat("#.###");

    public GeomeetrilineJaotus(double tõenäosus) {
        this.tõenäosus = tõenäosus;
    }

    public GeomeetrilineJaotus(double tõenäosus, int täpne) {
        this.tõenäosus = tõenäosus;
        this.täpne = täpne;
    }

    @Override
    public double Keskväärtus() {
        return Double.parseDouble(df.format(1/tõenäosus));
    }

    @Override
    public double Dispersioon() {
        return Double.parseDouble(df.format((1-tõenäosus)/Math.pow(tõenäosus,2)));
    }

    @Override
    public double Standardhälve() {
        return Double.parseDouble(df.format(Math.sqrt(this.Dispersioon())));
    }

    @Override
    public double Täpne() {
        return Double.parseDouble(df.format(Math.pow(1-tõenäosus, täpne - 1) * tõenäosus));
    }
}
