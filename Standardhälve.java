import java.util.List;

/**
 * Created by Villem Lassmann on 10/03/2017.
 */
public class Standardhälve implements Arvutused{
    private String nimi = "standardhälve";
    private Dispersioon disp = new Dispersioon();

    @Override
    public String getNimi() {
        return nimi;
    }

    @Override
    public double arvutus(List<Double> suurused, List<Double> tõenäosused){
        double tulemus;
        tulemus = Math.round(Math.sqrt((disp.arvutus(suurused, tõenäosused)))*1000);
        return tulemus/1000;
    }

}
