import java.text.DecimalFormat;

/**
 * Created by Villem on 01/05/2017.
 */
public class BinoomJaotus implements Jaotused {
    private int arv;
    private double tõenäosus;
    private int täpne;
    private DecimalFormat df = new DecimalFormat("#.###");

    public BinoomJaotus(int arv, double tõenäosus) {
        this.arv = arv;
        this.tõenäosus = tõenäosus;
    }

    public BinoomJaotus(int arv, double tõenäosus, int täpne) {
        this.arv = arv;
        this.tõenäosus = tõenäosus;
        this.täpne = täpne;
    }

    @Override
    public double Keskväärtus() {
        return Double.parseDouble(df.format(arv*tõenäosus));
    }

    @Override
    public double Dispersioon() {
        return Double.parseDouble(df.format(arv*tõenäosus*(1-tõenäosus)));
    }

    @Override
    public double Standardhälve() {
        return Double.parseDouble(df.format(Math.sqrt(this.Dispersioon())));
    }

    @Override
    public double Täpne() {
        DecimalFormat df = new DecimalFormat("#.###");
        double vastus = kombinatsioon(arv, täpne) * Math.pow(tõenäosus, täpne) * Math.pow(1-tõenäosus, täpne);
        return Double.parseDouble(df.format(vastus));
    }

    private long faktoriaal(int i){
        long kokku = 1;
        for(int k = i; k > 0; k--) {
            kokku = kokku*k;
        }
        System.out.println(kokku);
        return kokku;
    }

    private double kombinatsioon(int arv, int täpne){
        double kokku = (faktoriaal(arv)/(faktoriaal((arv-täpne))*faktoriaal(täpne)));
        return kokku;
    }
}
