/**
 * Created by Villem on 01/05/2017.
 */
public interface Jaotused {
    double Keskväärtus();
    double Dispersioon();
    double Standardhälve();
    double Täpne();
}
