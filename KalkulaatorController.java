import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import sun.plugin.javascript.navig.Anchor;

import java.net.URL;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Villem Lassmann on 01/05/2017.
 */
public class KalkulaatorController {
    @FXML
    private TextArea väljund;
    @FXML
    private AnchorPane Sisendid;
    @FXML
    private TextField HVäärtus;
    @FXML
    private TextField HLugeja;
    @FXML
    private TextField HNimetaja;
    @FXML
    private TextField KVäärtus;
    @FXML
    private TextField KTõenäosus;
    @FXML
    private TextField BArv;
    @FXML
    private TextField BTõen;
    @FXML
    private TextField BTäpne;
    @FXML
    private TextField GArv;
    @FXML
    private TextField GTõen;
    @FXML
    private TextField PArv;
    @FXML
    private TextField PLam;
    @FXML
    private Button sisesta;
    @FXML
    private Button sisesta1;

    private int lugeja =  1;
    private String algne = "";
    private List<Double> väärtused = new ArrayList<>();
    private List<Double> tõenäosused = new ArrayList<>();
    private Map<String, String> Aknad = new HashMap<>();{
        Aknad.put("Kümnendmurrud", "KymnendAken");
        Aknad.put("Harilikud murrud", "HarilikAken");
        Aknad.put("Binoomjaotus", "BinoomAken");
        Aknad.put("Geomeetriline jaotus", "GeomAken");
        Aknad.put("Poisson'i jaotus", "PoisAken");
    }


    @FXML
    public void arvutusVajutus(ActionEvent event){
        String nupp = ((Button) event.getSource()).getText();
        String aken = leiaAktiivne().getId();
        if(aken.equals("KymnendAken") || aken.equals("HarilikAken")){
            if(kontroll()){
                String vastus;
                switch (nupp){
                    case "Keskväärtus":
                        vastus = String.valueOf(new Keskväärtus().arvutus(väärtused, tõenäosused));
                        prindi("Jaotuse keskväärtus on: " + vastus);
                        reseti();
                        break;
                    case "Dispersioon":
                        vastus = String.valueOf(new Dispersioon().arvutus(väärtused, tõenäosused));
                        prindi("Jaotuse Dispersioon on: " + vastus);
                        reseti();
                        break;
                    case "Standardhälve":
                        vastus = String.valueOf(new Standardhälve().arvutus(väärtused, tõenäosused));
                        prindi("Jaotuse Standardhälve on: " + vastus);
                        reseti();
                        break;
                    case "Kõik":
                        vastus = String.valueOf(new Keskväärtus().arvutus(väärtused, tõenäosused));
                        prindi("Jaotuse keskväärtus on: " + vastus);
                        vastus = String.valueOf(new Standardhälve().arvutus(väärtused, tõenäosused));
                        prindi("Jaotuse Standardhälve on: " + vastus);
                        vastus = String.valueOf(new Dispersioon().arvutus(väärtused, tõenäosused));
                        prindi("Jaotuse Dispersioon on: " + vastus);
                        reseti();
                        break;
                }
            }
            else{
                prindi("Tõenäosuste summe ei võrdu ühega, proovi veel");
            }
        }
        else{
            try {
                switch (aken) {
                    case "BinoomAken":
                        binoomArvutused(nupp);
                        break;
                    case "GeomAken":
                        geomArvutused(nupp);
                        break;
                    case "PoisAken":
                        poisArvutused(nupp);
                        break;
                }
            }
            catch (IllegalArgumentException e){
                prindi(e.getMessage());
            }
        }
    }

    public void valikuNupuleVajutus(ActionEvent event){
        MenuItem valikuNupp = (MenuItem) (event.getSource());
        Valik(valikuNupp.getText());
    }

    public void Valik(String valik){
        väljadNulli(leiaAktiivne());
        leiaAktiivne().setVisible(false);
        for (Object laps: Sisendid.getChildren()){
            if (laps instanceof AnchorPane){
                if (((AnchorPane) laps).getId().equals(Aknad.get(valik))){
                    ((AnchorPane) laps).setVisible(true);
                }
            }
        }
    }

    public void reseti(){
        väärtused = new ArrayList<>();
        tõenäosused = new ArrayList<>();
    }

    public void resetiNupp(){
        reseti();
        väljund.setText("");
        lugeja = 1;
        algne = "";
        väljadNulli(leiaAktiivne());
    }

    public void sisesta(ActionEvent vajutus){
        Button vajutaja = (Button) vajutus.getSource();
        try {
            if (vajutaja == sisesta) {
                väärtused.add(Double.parseDouble(HVäärtus.getText()));
                tõenäosused.add(Double.parseDouble(HLugeja.getText()) / Double.parseDouble(HNimetaja.getText()));

            } else if (vajutaja == sisesta1) {
                väärtused.add(Double.parseDouble(KVäärtus.getText()));
                tõenäosused.add(Double.parseDouble(KTõenäosus.getText()));
            }
            prindi("Edukalt lisatud!");
            väljadNulli(leiaAktiivne());
        }
        catch (NumberFormatException e){
            prindi("Sisend on vale, proovi uuesti");
        }

    }

    public void prindi(String tekst){
        algne = algne + ("(" + lugeja + ")" + "  " + tekst + "\n");
        lugeja += 1;
        väljund.setText(algne);
        väljund.setScrollTop(Double.MAX_VALUE);
        //if(väljund.getChildren().size() > 15) väljund.getChildren().remove(0);
    }

    private boolean kontroll(){
        double kokku = 0;
        for (Double väärtus: tõenäosused){
            kokku += väärtus;
        }
        return (Math.abs(kokku - 1) < 0.01);
    }

    private void väljadNulli(AnchorPane paan){
        for (Node objekt: paan.getChildren()){
            if (objekt.getClass() == (new TextField()).getClass()){
                TextField valitud = (TextField) objekt;
                valitud.clear();
            }
        }
    }

    private AnchorPane leiaAktiivne(){
        for(Object laps: Sisendid.getChildren()){
            if (laps instanceof AnchorPane && ((AnchorPane) laps).isVisible()){
                return (AnchorPane) laps;
            }
        }
        return null;
    }

    private void binoomArvutused(String nupp){
        if(Double.parseDouble(BTõen.getText()) < 0 || Double.parseDouble(BTõen.getText()) > 1) {
            throw new IllegalArgumentException("Tõenäosus on valesti sisestatud.");
        }
        if(!BTäpne.getText().isEmpty() && Double.parseDouble(BArv.getText()) < Double.parseDouble(BTäpne.getText())){
            throw new IllegalArgumentException("Otsitav arv ei saa olla katsete arvust väiksem. ");
        }
        if(Double.parseDouble(BArv.getText()) <= 0){
            throw new IllegalArgumentException("Katsete arv on valesti sisestatud. ");
        }
        if(!BTäpne.getText().isEmpty() && Double.parseDouble(BTäpne.getText()) < 0){
            throw new IllegalArgumentException("Otsitav arv on valesti sisestatud. ");
        }

        BinoomJaotus BinJao;

        if(BTäpne.getText().isEmpty()){
            BinJao = new BinoomJaotus(Integer.parseInt(BArv.getText()), Double.parseDouble(BTõen.getText()));
        }
        else{
            BinJao = new BinoomJaotus(Integer.parseInt(BArv.getText()), Double.parseDouble(BTõen.getText()),
                    Integer.parseInt(BTäpne.getText()));
        }

        String vastus;
        switch (nupp) {
            case "Keskväärtus":
                vastus = String.valueOf(BinJao.Keskväärtus());
                prindi("Jaotuse keskväärtus on: " + vastus);
                reseti();
                break;
            case "Dispersioon":
                vastus = String.valueOf(BinJao.Dispersioon());
                prindi("Jaotuse Dispersioon on: " + vastus);
                reseti();
                break;
            case "Standardhälve":
                vastus = String.valueOf(BinJao.Standardhälve());
                prindi("Jaotuse Standardhälve on: " + vastus);
                reseti();
                break;
            case "Kõik":
                vastus = String.valueOf(BinJao.Keskväärtus());
                prindi("Jaotuse Keskväärtus on: " + vastus);
                vastus = String.valueOf(BinJao.Standardhälve());
                prindi("Jaotuse Standardhälve on: " + vastus);
                vastus = String.valueOf(BinJao.Dispersioon());
                prindi("Jaotuse Dispersioon on: " + vastus);
                reseti();
                break;
            case "Arvuta täpne":
                if (BTäpne.getText().isEmpty()) {
                    throw new IllegalArgumentException("Ei saa arvutada, kui pole arvu sisestatud");
                } else {
                    vastus = String.valueOf(BinJao.Täpne());
                    prindi("Arvu saamise tõenäosus on: " + vastus);
                }
                break;
        }
    }

    private void geomArvutused(String nupp){
        if(Double.parseDouble(GTõen.getText()) < 0 || Double.parseDouble(GTõen.getText()) > 1) {
            throw new IllegalArgumentException("Tõenäosus on valesti sisestatud. ");
        }
        if(!GArv.getText().isEmpty() && Double.parseDouble(GArv.getText()) < 0){
            throw new IllegalArgumentException("Otsitav arv on valesti sisestatud. ");
        }

        GeomeetrilineJaotus GeoJao;
        if(GArv.getText().isEmpty()){
            GeoJao = new GeomeetrilineJaotus(Double.parseDouble(GTõen.getText()));
        }
        else{
            GeoJao = new GeomeetrilineJaotus(Double.parseDouble(GTõen.getText()), Integer.parseInt(GArv.getText()));
        }

        String vastus;
        switch (nupp) {
            case "Keskväärtus":
                vastus = String.valueOf(GeoJao.Keskväärtus());
                prindi("Jaotuse keskväärtus on: " + vastus);
                reseti();
                break;
            case "Dispersioon":
                vastus = String.valueOf(GeoJao.Dispersioon());
                prindi("Jaotuse Dispersioon on: " + vastus);
                reseti();
                break;
            case "Standardhälve":
                vastus = String.valueOf(GeoJao.Standardhälve());
                prindi("Jaotuse Standardhälve on: " + vastus);
                reseti();
                break;
            case "Kõik":
                vastus = String.valueOf(GeoJao.Keskväärtus());
                prindi("Jaotuse Keskväärtus on: " + vastus);
                vastus = String.valueOf(GeoJao.Standardhälve());
                prindi("Jaotuse Standardhälve on: " + vastus);
                vastus = String.valueOf(GeoJao.Dispersioon());
                prindi("Jaotuse Dispersioon on: " + vastus);
                reseti();
                break;
            case "Arvuta täpne":
                if (GArv.getText().isEmpty()) {
                    throw new IllegalArgumentException("Ei saa arvutada, kui pole arvu sisestatud");
                } else {
                    vastus = String.valueOf(GeoJao.Täpne());
                    prindi("Arvu saamise tõenäosus on: " + vastus);
                }
                break;
        }
    }

    public void poisArvutused(String nupp){
        if(Double.parseDouble(PLam.getText()) < 0) {
            throw new IllegalArgumentException("Lambda on valesti sisestatud. ");
        }
        if(!PArv.getText().isEmpty() && Double.parseDouble(PArv.getText()) < 0){
            throw new IllegalArgumentException("Otsitav arv on valesti sisestatud. ");
        }

        PoissonJaotus PoiJao;
        if(PArv.getText().isEmpty()){
            PoiJao = new PoissonJaotus(Integer.parseInt(PLam.getText()));
        }
        else{
            PoiJao = new PoissonJaotus(Integer.parseInt(PLam.getText()), Integer.parseInt(PArv.getText()));
        }

        String vastus;
        switch (nupp) {
            case "Keskväärtus":
                vastus = String.valueOf(PoiJao.Keskväärtus());
                prindi("Jaotuse keskväärtus on: " + vastus);
                reseti();
                break;
            case "Dispersioon":
                vastus = String.valueOf(PoiJao.Dispersioon());
                prindi("Jaotuse Dispersioon on: " + vastus);
                reseti();
                break;
            case "Standardhälve":
                vastus = String.valueOf(PoiJao.Standardhälve());
                prindi("Jaotuse Standardhälve on: " + vastus);
                reseti();
                break;
            case "Kõik":
                vastus = String.valueOf(PoiJao.Keskväärtus());
                prindi("Jaotuse Keskväärtus on: " + vastus);
                vastus = String.valueOf(PoiJao.Standardhälve());
                prindi("Jaotuse Standardhälve on: " + vastus);
                vastus = String.valueOf(PoiJao.Dispersioon());
                prindi("Jaotuse Dispersioon on: " + vastus);
                reseti();
                break;
            case "Arvuta täpne":
                if (PArv.getText().isEmpty()) {
                    throw new IllegalArgumentException("Ei saa arvutada, kui pole arvu sisestatud");
                } else {
                    vastus = String.valueOf(PoiJao.Täpne());
                    prindi("Arvu saamise tõenäosus on: " + vastus);
                }
                break;
        }
    }
}
