import java.util.List;

/**
 * Created by Villem Lassmann on 09/03/2017.
 */
public class Keskväärtus implements Arvutused {
    private String nimi = "keskväärtus";

    public String getNimi() {
        return nimi;
    }

    public double arvutus(List<Double> suurused, List<Double> tõenäosused){
        double summa = 0;
        for (int i = 0; i < suurused.size(); i++){
            summa += suurused.get(i)*tõenäosused.get(i);
        }
        summa = Math.round((summa*1000));
        return summa/1000;
    }
}
